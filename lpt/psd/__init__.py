# Copyright (C) 2016  Leo Singer
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
from __future__ import division
__all__ = ('known_psds', 'from_pkg_data', 'from_lalsimulation',
           'from_expression')


import pkg_resources
import numpy as np
import numexpr as ne
import pycbc.psd
from pycbc.types import FrequencySeries


def _get_pkg_data_filenames():
    filenames = pkg_resources.resource_listdir(__name__, 'data')
    for filename in filenames:
        name, _, ext = filename.partition('.')
        if ext == 'asd.txt':
            is_asd_file = True
        elif ext == 'psd.txt':
            is_asd_file = False
        else:
            continue # skip file
        yield name, (filename, is_asd_file)
_pkg_data_filenames = dict(_get_pkg_data_filenames())


known_psds = list(sorted(list(_pkg_data_filenames.keys()) + pycbc.psd.analytical._psd_list))


def from_pkg_data(key, *args):
    filename, is_asd_file = _pkg_data_filenames[key]
    path = pkg_resources.resource_filename(__name__, 'data/' + filename)
    return pycbc.psd.from_txt(path, *args, is_asd_file=is_asd_file)


def from_lalsimulation(key, *args):
    try:
        psd = pycbc.psd.from_string(key, *args)
    except ValueError:
        raise KeyError
    return psd


class _lazy_psd_dict(object):
    """Dictionary of lazily-evaluated LALSimulation PSDs"""

    def __init__(self, *args):
        self._args = args
        self._cache = {}

    def __getitem__(self, key):
        # pycbc.psd.analytical.from_string raises a ValueError if the named
        # PSD does not exist. (FIXME: would be nicer if it raised a KeyError,
        # then we would not have to re-raise it as one here)
        try:
            try:
                ret = self._cache[key]
            except KeyError:
                try:
                    ret = from_lalsimulation(key, *self._args)
                except KeyError:
                    ret = from_pkg_data(key, *self._args)
                self._cache[key] = ret.data
        except KeyError:
            fmt = 'Could not find a PSD named "{0}". Built-in PSDs include:\n  {1}'
            raise KeyError(fmt.format(key, '\n  '.join(known_psds)))
        return ret


def from_expression(expr, length, delta_f, low_freq_cutoff):
    """Evaluate a PSD using an expression.

    Parameters
    ----------
    expr : string
        A Numpy-like expression in terms of f and any LALSimulation PSDs.
        For example: 'aLIGOZeroDetHighPower * 10 + f**2'
    length : int
        Length of the frequency series in samples.
    delta_f : float
        Frequency resolution of the frequency series.
    low_freq_cutoff : float
        Frequencies below this value are set to zero.

    Returns
    -------
    psd : FrequencySeries
        The generated frequency series.

    Examples
    --------

    >>> length = 16384
    >>> delta_f = 0.5
    >>> low_freq_cutoff = 35.0
    >>> f = np.arange(length, dtype=float) * delta_f
    >>> f[:int(low_freq_cutoff // delta_f)] = 0.0
    >>> args = (length, delta_f, low_freq_cutoff)
    >>> assert (from_expression('aLIGOZeroDetHighPower', *args) ==
    ...      from_lalsimulation('aLIGOZeroDetHighPower', *args))
    >>> assert np.all(
    ...         from_expression('aLIGOZeroDetHighPower * 10 + f', *args).data ==
    ...      from_lalsimulation('aLIGOZeroDetHighPower', *args).data * 10 + f)
    """
    # Locals: the frequency series f
    f = np.arange(length, dtype=float) * delta_f
    f[:int(low_freq_cutoff // delta_f)] = 0.0
    local_dict = {'f': f}

    # Globals: lazily evaluated LALSimulation PSDs
    global_dict = _lazy_psd_dict(length, delta_f, low_freq_cutoff)

    # Evaluate expression
    try:
        data = ne.evaluate(expr, local_dict, global_dict)
    except KeyError as e:
        raise ValueError('Error evaluating expression "{0}". {1}'.format(expr, e.message))

    # Done
    return FrequencySeries(data, delta_f=delta_f)
