from __future__ import absolute_import
from __future__ import division
"""
Draw sky localization ellipse plots
"""


import argparse
import matplotlib
matplotlib.use('pdf')
from lalinference.bayestar import command


def get_parser():
    parser = command.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('rb'),
                        metavar='INPUT.hdf5', help='output HDF5 file')
    parser.add_argument('output', type=argparse.FileType('wb'),
                        metavar='OUTPUT.pdf', help='output PDF file')
    parser.add_argument('--level', type=float, default=90,
                        help='Credible percentile [default: %(default)s]')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    #
    # Late imports
    #

    from astropy.table import Table, join
    import collections
    import logging
    from matplotlib import pyplot as plt
    import healpy as hp
    import numpy as np
    from reproject import reproject_from_healpix
    import lpt.config
    import h5py
    from .. import plot
    from lalinference.plot.backend_pdf_vcs_friendly import PdfPages
    from shapely import geos

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Open input and output files
    #

    log.info('reading from %s', args.input.name)
    table = join(
        Table.read(args.input.name, path='lpt/coincs'),
        Table.read(args.input.name, path='lpt/fisher_ellipses'))
    signal_models = Table.read(args.input.name, path='lpt/signal_models')

    with h5py.File(args.input.name, mode='r') as f:
        config_text = f['lpt'].attrs['config']
    config = lpt.config.load(config_text)

    max_distance = table['distance'].max()

    with PdfPages(args.output.name) as pdf:
        for run in collections.OrderedDict.fromkeys(table['run']):
            for template_id in collections.OrderedDict.fromkeys(table['template_id']):
                group = table[(table['run'] == run) & (table['template_id'] == template_id)]
                fig = plt.figure()
                ax = plt.axes(projection='lpt allsky')
                world_transform = ax.get_transform('world')
                ax.set_title('{0}, {1}'.format(group[0]['run'], group[0]['template_id']))

                # Plot heat map of distance
                npix = len(group)
                nside = hp.npix2nside(npix)
                distance = np.repeat(np.nan, npix)
                ipix = hp.ang2pix(nside, 0.5 * np.pi - group['lat'], group['lon'])
                distance[ipix] = group['distance']
                distance, _ = reproject_from_healpix((distance, 'icrs'), ax.header)
                im = ax.imshow(distance, origin='lower', cmap='GnBu',
                               vmin=0, vmax=table['distance'].max())
                cbar = plt.colorbar(im, orientation='horizontal', fraction=0.05)
                cbar.set_label('Distance (Mpc)')

                group_max_distance = group['distance'].max()
                for row in group:
                    try:
                        ax.add_patch(plot.DatelineClippedCovariancePatch(
                            row['lon'], row['lat'], row['pa'], row['a'], row['b'],
                            level=args.level, facecolor='none', edgecolor='black',
                            alpha=(row['distance'] / group_max_distance)**3,
                            linewidth=0.5, transform=world_transform))
                    except geos.TopologicalError:
                        # Skip some tricky polygon corner cases
                        log.exception('FIXME: skipping bad geometry')

                # Calculate positions and labels for detector range ticks
                horizons = join(Table(
                    rows=[(key, value['psd']) for key, value
                          in config['runs'][run]['detectors'].items()],
                    names=['detector', 'psd']), signal_models)['detector', 'r1']
                ticks = horizons['r1'] / (8 * 2.26)
                ticklabels = [_[0] for _ in horizons['detector']]
                ticks, ticklabels = plot.cluster_ticks(
                    ticks, ticklabels, vmin=0, vmax=max_distance)

                # Add detector range ticks
                cbar.ax.set_aspect('auto')
                cbar2 = cbar.ax.twiny()
                cbar2.set_xlim(cbar.get_clim())
                cbar2.set_xticks(ticks)
                cbar2.set_xticklabels(ticklabels)

                pdf.savefig(fig)
