from __future__ import absolute_import
from __future__ import division
"""
Generate sample sky maps for a LIGO/Virgo observing scenario
"""


import argparse
from lalinference.bayestar import command


def get_parser():
    parser = command.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'), default='-',
                        metavar='INPUT.yml', help='input YAML file')
    parser.add_argument('output', type=argparse.FileType('wb'),
                        metavar='OUTPUT.hdf5', help='output HDF5 file')
    parser.add_argument('--approximant', default='TaylorF2')
    parser.add_argument('--seed', type=int, default=0,
                        help='Random seed to make output deterministic')
    parser.add_argument('--snr-threshold', type=float, default=12.0,
                        help='network S/N threshold [default: %(default)s]')
    coherent_args = parser.add_mutually_exclusive_group()
    coherent_args.add_argument('--coherent', action='store_true', default=True,
                               help='Use amplitude and phase information in addition to timing [enabled by default]')
    coherent_args.add_argument('--incoherent', dest='coherent', action='store_false',
                               help='Use time information only')
    parser.add_argument('--nside', type=int, choices=[2**x for x in range(10)], default=4)
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    #
    # Late imports
    #

    import astropy.table
    import collections
    import healpy as hp
    import h5py
    import lal
    from lalinference.bayestar import filter
    from lalinference.bayestar import timing
    import lalsimulation
    import logging
    from .. import config, fisher, psd
    import numpy as np
    import pycbc.waveform

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Open input and output files
    #

    log.info('reading from %s', args.input.name)
    config = config.load(args.input)

    log.info('writing to %s', args.output.name)
    outfile = h5py.File(args.output.name)

    outgroup = outfile.create_group('lpt')
    outgroup.attrs['config'] = open(args.input.name).read()

    #
    # Look up Fisher matrix params
    #

    run_names = config['runs'].keys()
    log.info('runs: %s', ' '.join(run_names))

    psd_names = sorted(set(detector['psd'] for run in config['runs'].values() for detector in run['detectors'].values()))
    log.info('PSDs: %s', ' '.join(psd_names))

    astropy.table.Table(
        rows=[(run, detector, detector_params['psd']) for run, run_params in config['runs'].items() for detector, detector_params in run_params['detectors'].items()],
        names='run detector psd'.split()
    ).write(outfile, path='lpt/runs')

    astropy.table.Table(
        rows=[(template_id, template_params['mass1'], template_params['mass2']) for template_id, template_params in config['injections'].items()],
        names='template_id mass1 mass2'.split()).write(outfile, path='lpt/templates')

    detector_names = sorted(set(detector for run in config['runs'].values() for detector in run['detectors'].keys()))
    log.info('detectors: %s', ' '.join(detector_names))

    log.info('looking up detector geometry')
    detectors = {detector_name: lalsimulation.DetectorPrefixToLALDetector(detector_name) for detector_name in detector_names}
    detectors = {detector_name: (detector.response, detector.location / lal.C_SI) for detector_name, detector in detectors.items()}

    log.info('calculating single-detector Fisher matrices')
    flow = config['flow']
    fishers = collections.OrderedDict()
    for template_id, template_params in config['injections'].items():
        kwargs = dict(
            template_params, f_lower=flow, approximant=args.approximant)
        f_high = pycbc.waveform.get_waveform_end_frequency(**kwargs)
        f_sample = filter.ceil_pow_2(2 * f_high)
        duration = pycbc.waveform.get_waveform_filter_length_in_time(**kwargs)
        delta_f = f_sample / filter.ceil_pow_2(duration * f_sample)
        kwargs['delta_f'] = delta_f
        hplus, hcross = pycbc.waveform.get_fd_waveform(**kwargs)

        # Force `plus' and `cross' waveform to be in quadrature.
        h = 0.5 * (hplus.data + 1j * hcross.data)

        # Resize to nearest power of 2.
        h = np.concatenate((h, np.zeros(int(filter.ceil_pow_2(len(h))) - len(h))))

        for psd_name in psd_names:
            S = psd.from_expression(psd_name, len(h), delta_f, flow).data
            S[S == 0] = np.inf

            hs = filter.abs2(h) / S

            hseries = lal.CreateREAL8FrequencySeries(None, 0, 0, delta_f, lal.DimensionlessUnit, len(hs))
            hseries.data.data[:] = hs
            acor, sample_rate = filter.autocorrelation(hseries, 1)
            assert sample_rate == f_sample

            dw = 2 * np.pi * delta_f
            w = np.arange(len(h)) * dw
            w0 = np.sum(w**0 * hs) * dw
            r1 = np.asscalar(np.sqrt(4 / (2 * np.pi) * w0))
            w1 = np.asscalar(np.sum(w**1 * hs) * dw / w0)
            w2 = np.asscalar(np.sum(w**2 * hs) * dw / w0)

            fishers[(psd_name, template_id)] = (r1, w1, w2, sample_rate, acor)

    data = [(psd_name, template_id) + value for (psd_name, template_id), value in sorted(fishers.items())]
    astropy.table.Table(data=zip(*data), names='psd template_id r1 w1 w2 sample_rate acor'.split()).write(outfile, path='lpt/signal_models')

    log.info('calculating uncertainty ellipses')

    nside = args.nside
    npix = hp.nside2npix(nside)

    coinc_id = 0
    trigger_id = 0
    fisher_data = []
    coinc_data = []
    trigger_data = []
    for run_name, run in config['runs'].items():
        responses, locations, psd_names, network = zip(*(detectors[key] + (value['psd'], key) for key, value in run['detectors'].items()))
        network_str = ' '.join(network)
        for template_id in config['injections'].keys():
            r1s, w1s, w2s, _, _ = zip(*(fishers[(psd_name, template_id)] for psd_name in psd_names))
            for ipix in np.arange(npix):
                theta, phi = hp.pix2ang(nside, ipix)
                ra = phi
                dec = 0.5 * np.pi - theta
                inclination = 0.0
                polarization = 0.0
                distance = 1e3 # Arbitrary, because distance is rescaled below
                complex_snrs, pa, a, b = fisher.fisher_ellipse(ra, dec, polarization, inclination, distance, responses, locations, r1s, w1s, w2s, coherent=args.coherent)
                complex_snrs = np.asarray(complex_snrs)
                snr = np.sqrt(np.sum(filter.abs2(complex_snrs)))
                a *= snr / args.snr_threshold
                b *= snr / args.snr_threshold
                distance *= snr / args.snr_threshold
                complex_snrs *= args.snr_threshold / snr
                snr = args.snr_threshold
                coinc_data.append((coinc_id, run_name, network_str, template_id, ra, dec, inclination, polarization, distance, snr))
                fisher_data.append((coinc_id, pa, a, b))
                for detector, complex_snr in zip(network, complex_snrs):
                    trigger_data.append((trigger_id, coinc_id, detector, np.abs(complex_snr), np.angle(complex_snr)))
                    trigger_id += 1
                coinc_id += 1
    astropy.table.Table(rows=fisher_data, names='coinc_id pa a b'.split()).write(outfile, path='lpt/fisher_ellipses')
    astropy.table.Table(rows=coinc_data, names='coinc_id run detectors template_id lon lat inclination polarization distance network_snr'.split()).write(outfile, path='lpt/coincs')
    astropy.table.Table(rows=trigger_data, names='trigger_id coinc_id detector snr phase'.split()).write(outfile, path='lpt/triggers')

    outfile.close()
