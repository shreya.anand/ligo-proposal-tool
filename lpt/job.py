#
# Copyright (C) 2016  Leo Singer
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
from __future__ import print_function
"""
Simple fire-and-forget submission of Condor jobs to call Python functions.
"""


from subprocess import check_output, CalledProcessError
from cPickle import loads, dumps, HIGHEST_PROTOCOL
from base64 import b64encode, b64decode
from sys import argv, executable, stderr
__all__ = ('submit',)


def submit(func, *args, **kwargs):
    """Submit a Condor job to call function `func`."""
    payload = (func, args, kwargs)
    payload = b64encode(dumps(payload, HIGHEST_PROTOCOL)).replace('=', '_')
    cmd = ['condor_submit', 'accounting_group=ligo.sim.o3.cbc.explore.test',
           'universe=vanilla', 'getenv=true', 'executable=' + executable,
           'request_memory="2000 MB"',
           'arguments="-m ' + __name__ + ' ' + payload + '"',
           '-append', 'queue 1', '/dev/null']
    try:
        check_output(cmd)
    except CalledProcessError:
        print('error: condor_submit failed', file=stderr)
        print(output)
        raise


if __name__ == '__main__':
    func, args, kwargs = loads(b64decode(argv[1].replace('_', '=')))
    func(*args, **kwargs)
