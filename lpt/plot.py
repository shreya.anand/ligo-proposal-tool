# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Leo Singer
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""All-sky plots"""
from __future__ import division
__all__ = ('AllSkyAxes', 'cluster_ticks', 'CovariancePatch',
           'DatelineClippedCovariancePatch')

import itertools
from lalinference.plot import cut_dateline
from astropy.coordinates import Angle, cartesian_to_spherical
from astropy.io.fits import Header
import astropy.units as u
from astropy.wcs import WCS
from astropy.visualization.wcsaxes.core import WCSAxes
from astropy.visualization.wcsaxes.frame import EllipticalFrame
from matplotlib import patches
from matplotlib.path import Path
from matplotlib.projections import projection_registry
from matplotlib import transforms
from scipy.cluster.hierarchy import fclusterdata
import numpy as np
import healpy as hp
from shapely import geometry


class AllSkyAxes(WCSAxes):
    """A good multi-purpose all-sky projection"""

    _header = Header((
        ('NAXIS',  2),
        ('NAXIS1', 360),
        ('NAXIS2', 180),
        ('CRPIX1', 180.0),
        ('CRPIX2', 90.0),
        ('CRVAL1', 0.0),
        ('CRVAL2', 0.0),
        ('CDELT1', 2 * np.sqrt(2) / np.pi),
        ('CDELT2', 2 * np.sqrt(2) / np.pi),
        ('CTYPE1', 'RA---AIT'),
        ('CTYPE2', 'DEC--AIT'),
        ('RADESYS', 'ICRS')))

    _wcs = WCS(_header)

    name = 'lpt allsky'

    def __init__(self, *args, **kwargs):
        super(AllSkyAxes, self).__init__(
            *args, aspect=1, wcs=self._wcs, frame_class=EllipticalFrame,
            **kwargs)
        self.set_xlim(0, self.header['NAXIS1'] - 1)
        self.set_ylim(0, self.header['NAXIS2'] - 1)
        self.coords[0].set_ticks(spacing=30*u.deg)
        self.coords[1].set_ticks(spacing=15*u.deg)
        self.grid()

    @property
    def header(self):
        return self._header

projection_registry.register(AllSkyAxes)


def _coerce_to_rad(angle):
    """Convert angle to degrees, assuming it is units of radians if its units
    are unspecified."""
    return Angle(angle).rad if hasattr(angle, 'unit') else angle


def _path_to_vertices(path):
    for i, (points, code) in enumerate(zip(path.vertices, path.codes)):
        if i == 0 and code == Path.MOVETO:
            continue
        elif code == Path.CLOSEPOLY:
            continue
        elif code != Path.LINETO:
            raise ValueError('Unexpected path code')
        else:
            yield points


def _vertices_to_path(polys):
    points = np.row_stack([
        np.row_stack((poly[0], poly[1:], poly[-1]))
        for poly in polys])
    codes = np.hstack([
        np.hstack((Path.MOVETO, np.repeat(Path.LINETO, len(poly) - 1), Path.CLOSEPOLY))
        for poly in polys])
    return Path(points, codes)


class CovariancePatch(patches.Patch):
    """Matplotlib patch for representing a covariance ellipse."""

    _npts = 40
    _phi = 2 * np.pi / _npts * np.arange(_npts)
    _cosphi = np.cos(_phi)
    _sinphi = np.sin(_phi)

    @property
    def lon(self):
        return self._lon
    @lon.setter
    def lon(self, value):
        self._lon = _coerce_to_rad(value)

    @property
    def lat(self):
        return self._lat
    @lat.setter
    def lat(self, value):
        self._lat = _coerce_to_rad(value)

    @property
    def pa(self):
        return self._pa
    @pa.setter
    def pa(self, value):
        self._pa = _coerce_to_rad(value)

    @property
    def a(self):
        return self._a
    @a.setter
    def a(self, value):
        self._a = _coerce_to_rad(value)

    @property
    def b(self):
        return self._b
    @b.setter
    def b(self, value):
        self._b = _coerce_to_rad(value)

    def __init__(self, lon, lat, pa, a, b, level=90, **kwargs):
        """
        Parameters
        ----------
        All angles are assumed to be in units of radians if left unspecified.

        lon : float or `astropy.units.Quantity`
            Central longitude
        lat : float or `astropy.units.Quantity`
            Central latitude
        pa : float or `astropy.units.Quantity`
            Position angle of semi-major axis
        a : float or `astropy.units.Quantity`
            Standard deviation along semi-major axis
        b : float or `astropy.units.Quantity`
            Standard deviation along semi-minor axis
        level : float
            Desired confidence level in percent
        kwargs : dict
            Additional Matplotlib patch keyword arguments
        """
        super(CovariancePatch, self).__init__(**kwargs)
        self.lon = lon
        self.lat = lat
        self.pa = pa
        self.a = a
        self.b = b
        self.level = level

    def get_path(self):
        theta = 0.5 * np.pi - self.lat
        phi = self.lon
        scale = np.sqrt(-2 * np.log(1 - 0.01 * self.level))
        x = self.a * scale * self._cosphi
        y = self.b * scale * self._sinphi
        z2 = 1 - np.square(x) - np.square(y)

        if np.any(z2 < 0):
            # FIXME: don't know how to handle degenerate case yet.
            # Should implement Kent distribution credible levels.
            return Path(np.zeros((0, 2)))

        z = np.sqrt(z2)
        xyz = np.row_stack((x, y, z))
        R = hp.rotator.euler_matrix_new(self.pa, theta, np.pi - phi, deg=False, Y=True)
        _, lat, lon = cartesian_to_spherical(*np.dot(R, xyz))
        xy = np.column_stack((lon.deg, lat.deg))
        return _vertices_to_path([xy])


class DatelineClippedCovariancePatch(CovariancePatch):

    def get_path(self):
        path = super(DatelineClippedCovariancePatch, self).get_path()
        if len(path.vertices) > 0:
            # Only proceed if non-empty.
            vertices = np.deg2rad(list(_path_to_vertices(path)))
            vertices = [np.rad2deg(v) for v in cut_dateline(vertices)]
            path = _vertices_to_path(vertices)
        return path


def cluster_ticks(ticks, ticklabels, vmin=0.0, vmax=1.0, fraction=0.01, sep=''):
    """Cluster nearby tick labels.

    Parameters
    ----------
    ticks : list
        List of tick positions
    ticklabels : list
        List of tick labels
    vmin : float
        Minimum value of axis
    vmax : float
        Maximum value of axis
    fraction : float
        Threshold fraction of length of axis for grouping tick labels
    sep : str
        String to insert between tick labels

    Returns
    -------
    ticks : list
        List of tick positions
    ticklabels : list
        List of clustered tick labels
    """
    # Cluster nearby tick labels
    data = np.reshape(ticks, (-1, 1))
    threshold = fraction * (vmax - vmin)
    clusters = fclusterdata(data, threshold, criterion='distance')

    # Concatenate nearby tick labels
    keyfunc = lambda _: _[0]
    cleaned_ticklabels = []
    for _, group in itertools.groupby(zip(clusters, ticklabels), keyfunc):
        labels = [_[1] for _ in group]
        cleaned_ticklabels.append(sep.join(labels))
        cleaned_ticklabels.extend([''] * (len(labels) - 1))

    # Done!
    return ticks, cleaned_ticklabels
