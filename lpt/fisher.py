#!/usr/bin/env python
#
# Copyright (C) 2014-2017  Leo Singer
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""Calculate sky areas using Fisher matrices. Adapted from the appendices of:

Singer, L.P., 2015, "The needle in the hundred square degree haystack:
    The hunt for binary neutron star mergers with LIGO and Palomar Transient
    Factory", Ph.D. Thesis, California Institute of Technology,
    http://thesis.library.caltech.edu/8739/

Singer, L.P. and Price, L.R., 2016, "Rapid Bayesian position reconstruction for
    gravitational-wave transients", PRD 93: 024013,
    http://adsabs.harvard.edu/abs/2016PhRvD..93b4013S
"""
from __future__ import division
from __future__ import print_function
__all__ = ('ellipse_for_information', 'fisher_ellipse', 'marginal_information')


import numpy as np
import scipy.stats
import scipy.linalg


def exp_i(phi):
    return np.cos(phi) + np.sin(phi) * 1j


def signal_model(psi, u, r):
    """Polarization for phic=0 (arbitrary?)"""
    phic = 0
    cos2psi = np.cos(2*psi)
    sin2psi = np.sin(2*psi)
    f = 0.5 * (1 + np.square(u))
    pre = exp_i(2*phic) / r
    hp = pre * (f * cos2psi + 1j * u * sin2psi)
    hc = pre * (f * sin2psi - 1j * u * cos2psi)
    return hp, hc


def radiation_to_earth_rotation(phi, theta):
    """Active transformation from radiation to Earth frame.

    Test that Astropy spherical-to-cartesian transformation matches.
    >>> from astropy import coordinates as c
    >>> from astropy import units as u
    >>> np.random.seed(0)
    >>> n = 100
    >>> theta = np.arccos(np.random.uniform(-1, 1, size=n))
    >>> phi = np.random.uniform(0, 2 * np.pi, size=n)
    >>> coord = c.SkyCoord(r=1, theta=theta*u.rad, phi=phi*u.rad,
    ...                    representation=c.PhysicsSphericalRepresentation)
    >>> desired = np.asarray(coord.cartesian.xyz).T
    >>> Rs = [radiation_to_earth_rotation(ph, th) for th, ph in zip(theta, phi)]
    >>> Rs = np.asarray(Rs)
    >>> actual = Rs[:, :, 2]
    >>> np.testing.assert_allclose(actual, desired)

    Test that LAL antenna patterns match.
    FIXME: The Fx component differs in sign.
    >>> import lal
    >>> import lalsimulation
    >>> ra = phi
    >>> dec = 0.5 * np.pi - theta
    >>> response = lalsimulation.DetectorPrefixToLALDetector('H1').response
    >>> desired = [lal.ComputeDetAMResponse(response, ra_, dec_, 0, 0)
    ...            for ra_, dec_ in zip(ra, dec)]
    >>> D = np.asarray([np.dot(np.dot(R.T, response), R) for R in Rs])
    >>> actual = np.column_stack((D[:, 0, 0] - D[:, 1, 1], -2 * D[:, 0, 1]))
    >>> np.testing.assert_allclose(actual, desired)

    Test that the rotation matrix has a determinant of 1.
    >>> for R in Rs:
    ...     np.testing.assert_allclose(np.linalg.det(R), 1)
    """
    c1 = np.cos(phi)
    c2 = np.cos(theta)
    s1 = np.sin(phi)
    s2 = np.sin(theta)
    return np.asarray(
        [[-s1, -c1*c2, c1*s2],
         [ c1, -s1*c2, s1*s2],
         [  0,     s2,    c2]])


def condition(r, locations, r1s, w1s, w2s):
    """Rescale distance and time units."""
    wbar = np.sqrt(np.mean(w2s))
    rbar = scipy.stats.gmean(np.concatenate(([r], r1s)))
    return (
        r / rbar,
        np.asarray(locations) * wbar,
        r1s / rbar,
        w1s / wbar,
        w2s / np.square(wbar))


def fisher_matrix_term(hp, hc, R, response, location, r1, w1, w2,
                       coherent=True):
    """Single-detector Fisher matrix."""
    # Transformed detector response tensor and position
    D = r1 * np.dot(np.dot(R.T, response), R)
    d = np.dot(R.T, location)

    # Amplitude
    z = hp*(D[0, 0] - D[1, 1]) + 2 * hc*D[0, 1]
    a = np.real(z)
    b = np.imag(z)
    snr2 = np.square(a) + np.square(b)

    if coherent:
        # Jacobian matrix
        dz_dph = -2 * hp * D[0, 2] - 2 * hc * D[1, 2]
        dz_dth = -2 * hc * D[0, 2] + 2 * hp * D[1, 2]
        dz_drehp = D[0, 0] - D[1, 1]
        dz_drehc = 2 * D[0, 1]
        J = np.asarray([
             [np.real(dz_dph), np.real(dz_dth), dz_drehp, 0, dz_drehc, 0, 0],
             [np.imag(dz_dph), np.imag(dz_dth), 0, dz_drehp, 0, dz_drehc, 0],
             [-d[0], -d[1], 0, 0, 0, 0, 1]])

        # Single-detector information matrix
        I = np.asarray([
             [1, 0, w1*b],
             [0, 1, -w1*a],
             [w1*b, -w1*a, snr2*w2]])
    else:
        # Jacobian matrix
        J = np.asarray([[-d[0], -d[1], -d[2], 1]])

        # Single-detector information matrix (well, scalar, in this case)
        I = snr2 * (w2 - np.square(w1))

    return np.dot(np.dot(J.T, I), J), z


def marginal_information(I, n):
    """Given an information matrix I, find the information matrix for the
    marginal distribution of just the first n parameters by using
    partitioned matrix inversion (the Schur complement).

    >>> np.random.seed(0)
    >>> I = np.random.normal(size=(5, 5))
    >>> I += I.T
    >>> I += np.eye(5)
    >>> desired = scipy.linalg.inv(scipy.linalg.inv(I)[:2, :2])
    >>> actual = marginal_information(I, 2)
    >>> np.testing.assert_allclose(actual, desired)
    """
    A = I[:n, :n]
    B = I[:n, n:]
    C = I[n:, n:]
    return A - np.dot(B, scipy.linalg.solve(C, B.T, sym_pos=True))


def fisher_matrix(phi, theta, psi, u, r, responses, locations, r1s, w1s, w2s,
                  coherent=True):
    """Multi-detector Fisher matrix."""
    r, locations, r1s, w1s, w2s = condition(r, locations, r1s, w1s, w2s)
    hp, hc = signal_model(psi, u, r)
    R = radiation_to_earth_rotation(phi, theta)

    # Loop over detectors
    terms, complex_snrs = zip(*(
        fisher_matrix_term(hp, hc, R, D, d, r1, w1, w2, coherent=coherent)
        for D, d, r1, w1, w2
        in zip(responses, locations, r1s, w1s, w2s)))

    # Total information
    I = np.sum(terms, axis=0)

    # Marginalize over nuisance parameters
    I = marginal_information(I, 2)

    return I, complex_snrs


def ellipse_for_information(I):
    """Calculate parameters of an uncertainty ellipse from the 2D
    information matrix.

    Parameters
    ----------
    I : `numpy.ndarray`
        2x2 information matrix

    Returns
    -------
    pa : float
        Position angle of semi-major axis in radians
    a : float
        Semi-major axis in radians
    b : float
        Semi-minor axis in radians

    >>> std = [2, 1]
    >>> C = np.diag(np.square(std))
    >>> I = scipy.linalg.inv(C)
    >>> pa, a, b = ellipse_for_information(I)
    >>> np.testing.assert_allclose([a, b], sorted(std, reverse=True))
    >>> np.testing.assert_allclose(pa, -np.pi / 2)

    >>> std = [1, 2]
    >>> C = np.diag(np.square(std))
    >>> I = scipy.linalg.inv(C)
    >>> pa, a, b = ellipse_for_information(I)
    >>> np.testing.assert_allclose([a, b], sorted(std, reverse=True))
    >>> np.testing.assert_allclose(pa, np.pi)
    """
    W, V = scipy.linalg.eigh(I)
    a, b = 1 / np.sqrt(W)
    pa = 0.5 * np.pi - np.arctan2(V[1, 0], V[0, 0])
    return pa, a, b


def fisher_ellipse(ra, dec, polarization, inclination, distance,
                   responses, locations, horizons, w1s, w2s, coherent=True):
    """Calculate error ellipse from Fisher matrix."""
    theta = 0.5 * np.pi - dec
    phi = ra
    u = np.cos(inclination)
    I, complex_snrs = fisher_matrix(phi, theta, polarization, u, distance,
                                    responses, locations, horizons, w1s, w2s,
                                    coherent=coherent)
    pa, a, b = ellipse_for_information(I)
    return complex_snrs, pa, a, b
