# LIGO Proposal Tool

Exploratory analysis and automation of sky localization studies for
gravitational-wave detector networks.

See http://ligo-proposal-tool.readthedocs.io/ for documentation, and
http://ligo-proposal-tool.readthedocs.io/en/latest/install.html for
installation instructions.

## Related packages/modules/scripts

### LALInference

 * `bayestar_sample_model_psd`: synthesize PSDs using models in LALSimulation
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/bayestar_sample_model_psd.py
 * `bayestar_realize_coincs`: zero-noise simulation of detection pipeline
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/bayestar_realize_coincs.py
 * `bayestar_localize_coincs`: BAYESTAR batch mode processing
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/bayestar_localize_coincs.py
 * `lalinference.bayestar.timing`: PSD conditioning, horizon distance, Fisher timing formulae
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/lalinference/bayestar/timing.py
 * `lalinference.bayestar.filter`: template realization, whitening, autocorrelation functions
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/lalinference/bayestar/filter.py
 * `bayestar_sky_map.h`, `bayestar_sky_map.c`: BAYESTAR compute code, plain C, no LAL types
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/src/bayestar_sky_map.h
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/src/bayestar_sky_map.c
 * `lalinference.bayestar._sky_map`: Python C API wrapper for `bayestar_sky_map.h`
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/lalinference/bayestar/_sky_map.c
 * `lalinference.bayestar.sky_map`: LIGO_LW, Glue, LAL/SWIG flavored driver for BAYESTAR
   https://github.com/lscsoft/lalsuite/blob/master/lalinference/python/lalinference/bayestar/sky_map.py

### PyCBC

 * PSD utilities (in particular the `from_cli*()` functions in `__init__.py`)
   https://github.com/ligo-cbc/pycbc/tree/master/pycbc/psd
   Example: https://github.com/ligo-cbc/pycbc/blob/master/bin/pycbc_optimal_snr
 * Filter module (in particular the resample and autocorrelation submodules)
   https://github.com/ligo-cbc/pycbc/tree/master/pycbc/filter
 * Waveform generation
   https://github.com/ligo-cbc/pycbc/blob/master/pycbc/waveform/waveform.py
   Example: https://github.com/ligo-cbc/pycbc/blob/master/bin/plotting/pycbc_plot_waveform
 * FFT
   https://github.com/ligo-cbc/pycbc/tree/master/pycbc/fft
 * Array, time and frequency series
   https://github.com/ligo-cbc/pycbc/tree/master/pycbc/types
 * Metric and moment calculation for template banks
   https://github.com/ligo-cbc/pycbc/blob/master/pycbc/tmpltbank/calc_moments.py
 * PyCBC documentation
   http://ligo-cbc.github.io/pycbc/latest/html/

### Other

 * `fisher_areas.py`: coherent Fisher sky areas with timing, amplitude, and phase
   http://thesis.library.caltech.edu/8739/ Appendix A.6
